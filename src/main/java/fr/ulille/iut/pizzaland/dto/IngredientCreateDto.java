package fr.ulille.iut.pizzaland.dto;

import java.net.URI;

import javax.ws.rs.POST;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class IngredientCreateDto {
	private String name;

	public IngredientCreateDto() {}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "IngredientCreateDto [name=" + name + "]";
	}
}
